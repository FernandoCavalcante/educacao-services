using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;

namespace Domain.Repositories
{
    public interface IUserRepository
    {
        Task<int> CreateUser(User user);
        Task<User> GetUser(string username, string password);
        Task<User> GetUser(string username);
        Task<IEnumerable<User>> GetAllUsers();
    }
}