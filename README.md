# Serviço para aplicação de educação

Backend da aplicação de complementação de ensino

## Pré-requisitos
> .NET Core SDK 3.1 ou superior

## Build do serviço

Abra o terminal da pasta da solution e digite o seguinte comando:

```
dotnet build
```

## Rodando a aplicação

Com o terminal ainda na pasta da solution, digite:


``
dotnet run --project Application/Application.csproj
``

Ou navegue até a pasta **Application** e digite:

``
dotnet run
``