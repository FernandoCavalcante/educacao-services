using Domain.Models;
using Domain.Services.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class TokenService : ITokenService
    {
        private IConfiguration configuration;
        public TokenService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<string> GenerateToken(User user)
        {
            var handler = new JwtSecurityTokenHandler();
            var personalKey = configuration.GetSection("TokenKey").ToString();
            var key = Encoding.ASCII.GetBytes(personalKey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                                                 {
                                                    new Claim(ClaimTypes.Name, user.Username.ToString()),
                                                    new Claim(ClaimTypes.Role, user.Role.ToString())
                                                 }),
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                                                            SecurityAlgorithms.HmacSha256Signature)
            };
            
            var token = await Task.Run(() => handler.CreateToken(tokenDescriptor));

            return handler.WriteToken(token);
        }
    }
}