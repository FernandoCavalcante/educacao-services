using System.Threading.Tasks;
using Domain.Models;
using Domain.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Application.Controllers
{
    ///<sumary>
    /// User CRUD endpoints
    ///</sumary>
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository repository;

        ///<sumary>
        /// Constructor with Dependency Injection
        ///</sumary>
        public UserController(IUserRepository repository)
        {
            this.repository = repository;
        }

        ///<sumary>
        /// User register endpoint
        ///</sumary>
        ///<param name="userRegister">User to be registered</param>
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] User userRegister)
        {
            var user = await repository.CreateUser(userRegister);
            return Ok("Usuário cadastrado com sucesso.");
        }

        ///<sumary>
        /// Get all users endpoint
        ///</sumary>
        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            return Ok(await repository.GetAllUsers());
        }

        ///<sumary>
        /// Get user by username
        ///</sumary>
        ///<param name="username">User who will be found </param>
        [HttpGet]
        public async Task<IActionResult> GetUserByUsername(string username)
        {
            return Ok(await repository.GetUser(username));
        }
    }
}