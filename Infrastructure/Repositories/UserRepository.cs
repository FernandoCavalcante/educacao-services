using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using Domain.Models;
using Domain.Repositories;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace Infrastructure.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IConfiguration configuration;
        public UserRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        
        public async Task<int> CreateUser(User user)
        {
            using (var con = new MySqlConnection(configuration.GetConnectionString("EducacaoDb")))
                return await con.InsertAsync<User>(user);
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            using (var con = new MySqlConnection(configuration.GetConnectionString("EducacaoDb")))
                return await con.GetAllAsync<User>();
        }

        public async Task<User> GetUser(string username, string password)
        {
            using (var con = new MySqlConnection(configuration.GetConnectionString("EducacaoDb")))
                return await con.QueryFirstOrDefaultAsync<User>($"SELECT * FROM `User` WHERE Username = '{username}'");
        }

        public async Task<User> GetUser(string username)
        {
            using (var con = new MySqlConnection(configuration.GetConnectionString("EducacaoDb")))
                return await con.QueryFirstOrDefaultAsync<User>($"SELECT * FROM `User` WHERE Username = '{username}'");
        }
    }
}