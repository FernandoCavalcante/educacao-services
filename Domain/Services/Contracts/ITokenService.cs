using System.Threading.Tasks;
using Domain.Models;

namespace Domain.Services.Contracts
{
    public interface ITokenService
    {
        Task<string> GenerateToken(User user);
    }
}