using System.IO;
using System.Text;
using Domain.Repositories;
using Domain.Services;
using Domain.Services.Contracts;
using Infrastructure.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Linq;

namespace Application
{
    ///<sumary></sumary>
    public class Startup
    {
        ///<sumary></sumary>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        ///<sumary></sumary>
        public IConfiguration Configuration { get; }

        ///<sumary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        ///</sumary>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(action =>
                action.AddPolicy("allowOrigins", builder =>
                builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()));

            #region Dependency Injection
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ITokenService, TokenService>();
            #endregion

            #region JWT Configuration
            var personalKey = Configuration.GetSection("TokenKey").ToString();
            var key = Encoding.ASCII.GetBytes(personalKey);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            #endregion

            #region Swagger Configuration
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "Educacao Services",
                        Version = "v1"
                    });

                var applicationPath =
                    PlatformServices.Default.Application.ApplicationBasePath;
                var applicationName =
                    PlatformServices.Default.Application.ApplicationName;
                var xmlDocPath =
                    Path.Combine(applicationPath, $"{applicationName}.xml");

                c.IncludeXmlComments(xmlDocPath);
                c.ResolveConflictingActions(x => x.First());
            });
            #endregion

            services.AddControllers();
        }


        ///<sumary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        ///</sumary>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("allowOrigins");

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
                        {
                            endpoints.MapControllers();
                        });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "educacao");
            });
        }
    }
}
