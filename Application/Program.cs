using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Application
{
    ///<sumary></sumary>
    public class Program
    {
        ///<sumary></sumary>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        ///<sumary></sumary>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
