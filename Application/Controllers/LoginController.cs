using System.Threading.Tasks;
using Domain.Models;
using Domain.Repositories;
using Domain.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace Application.Controllers
{
    ///<sumary>
    /// User authentication endpoints
    ///</sumary>
    [Route("[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ITokenService service;
        private readonly IUserRepository repository;

        ///<sumary>
        /// Constructor with Dependency Injection
        ///</sumary>
        public LoginController(ITokenService service, IUserRepository repository)
        {
            this.service = service;
            this.repository = repository;
        }

        ///<sumary>
        /// User login endpoint
        ///</sumary>
        ///<param name="userLogin">User to be authenticated</param>
        ///<returns>User authenticated and its JWT</returns>
        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody] UserLogin userLogin)
        {
            var user = await repository.GetUser(userLogin.Username, userLogin.Password);

            if (user is null)
                return NotFound("Usuário ou senha inválidos");

            var token = await service.GenerateToken(user);

            user.Password = string.Empty;

            return Ok(new
            {
                user,
                token
            });
        }
    }
}